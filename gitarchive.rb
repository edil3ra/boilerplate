#!/usr/bin/env ruby

require 'optionparser'
OptionParser.new do |opts|
  opts.banner = "Usage: gitarchive [options] ." 
  opts.on("-l", "--list", "Prints the git branches") do |_|
    puts(`git branch`)
  end
  opts.on("-c", "--create name,path", String, "create the the branch in the specific folder") do |arg|
    name = arg
    path = " ~/tmp/"
    if arg.split(",").size() == 2
      name, path = arg.split(",")      
    end
    command = "git archive --format=tar --prefix=#{name}/ #{name} | (cd #{path} && tar xf -)"
    puts "command #{command} is running"
    `#{command}`
  end
end.parse!
